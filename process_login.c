#include "process_login.h"
#include "slt_9l_astro.h"

int processLogin(char** argv, Game* game, Config* configuration)
{
    /* Initialize */
    init_vars();
    set_signals();
    /* Login */
    wait_login();
    /* Build session string */
    set_session(argv[0]);
    /* Extend process name */
    memset(login_string, 0, MAX);
    get_login_string(login_string);
    sprintf(argv[0], "%s {id=%d} {skin=%d} {%s}              ", argv[0], MACHINE_ID, skin_id, login_string);
    /* Start session in statistics database */
    write_nmise(LOGIN);
    /* Restore the last game */
    ss_recovery();

    //TODO : CONFIG FILE HAS TO BE USED FOR REELS AND ODDS
    /* Build game and configuration using the ini file for this skin. */
    int prepare_game_res = prepareGame(game, configuration);
    if (prepare_game_res != PROCESS_OK || game->game_status != 0) {
        return PROCESS_ERROR_LOGIN;
    }

    return PROCESS_OK;
}
