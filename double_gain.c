#include <isbc/log.h>
#include "double_gain.h"

/******************************************
      DEFINES
******************************************/



/******************************************
      STATIC FONCTIONS
******************************************/





/******************************************
      FUNCTIONS
******************************************/

/**
 * \brief Executes the game process.
 * \param game game values
 * \param configuration config values
 * \return 0 if OK
 */
/* ==================================================================================================
    GAMBLE GAIN
================================================================================================== */
void double_gain() {
  int cmd, double_colour, double_selected, i;
  doubles++;  // set current double phase
  // read player's option
  cmd = read_cmd(0);
  // check if the choice is in the defined range
  if (cmd != 0 && cmd != 1)
    general_error(ERROR_INVALID_VALUE, "invalid choice read from client during double or collect");
  double_selected = cmd;
  //!!!!!!only for test server
  if ((strncmp(TESTUSER, login_string, strlen(TESTUSER)) == 0) && (double_selected == 1)) { // test user
    double_colour = double_selected;
    // save it in the array the prevoius colour
    for (i=SAVED_COLOURS -1; i>0; i--)
      last_colours[i] = last_colours[i-1];
    last_colours[0] = double_colour;
    memset(screen, 0, MAX);
    sprintf(screen, "%d:%d", double_selected, double_colour);
    total_gain *= 2;
    if (doubles == 5) {
      limit = 1;  // win and limit
      win(1);     // send the gain to client
    }
    else {
      write_nmise(WON_AND_CHOICE);
      ss_state = STATE_DOUBLE_CHOICE;
      send_data(ss_state);
    }
  }
  else { //end test
    if (genrand_range(0,99)<50)
      double_colour = double_selected;
    else
      double_colour = 1-double_selected;
    // save it in the array the previous colour
    for (i=SAVED_COLOURS -1; i>0; i--)
      last_colours[i] = last_colours[i-1];
    last_colours[0] = double_colour;

    memset(screen, 0, MAX);
    sprintf(screen, "%d:%d", double_selected, double_colour);

    if (double_selected != double_colour) {
      total_gain = 0;
      lose(1);
    }  
    else {
      total_gain *= 2;
      if (doubles == 5) {
        limit = 1;  // win and limit
        win(1);     // send the gain to client
      }  
      else {
        write_nmise(WON_AND_CHOICE);
        ss_state = STATE_DOUBLE_CHOICE;
        send_data(ss_state);
      }
    }
  }
}