#include <isbc/log.h>
#include <isbc/xml.h>
#include "mechanics.h"

/******************************************
      DEFINES
******************************************/



/******************************************
      STATIC FONCTIONS
******************************************/





/******************************************
      FUNCTIONS
******************************************/

/**
 * \brief Executes the game process.
 * \param game game values
 * \param configuration config values
 * \return 0 if OK
 */

/* ==================================================================================================
     SEND DATA TO CLIENT
================================================================================================== */


static void add_endgame_info(char* destiny, level) {
    int i;

    xml_add_int(destiny, level, "money", player_money);

    char combination_xml[XMLMAX] = "";
    for (i=0; i<ROWS; i++) {
        char buf[XMLMAX] = "";
        sprintf(buf, "%d-%d-%d-%d-%d", combination[i][0], combination[i][1], combination[i][2], combination[i][3], combination[i][4]);
        xml_add_str(combination_xml, level + 1, "line", buf);
    }
    xml_add_node(destiny, level, "symbols", xml_temp);

    char wlines_xml[XMLMAX] = "";
    for (i=0; i<LINES; i++) {
        if (lines_gain[i]) {
            char line_xml[XMLMAX] = "";
            xml_add_int(line_xml, level + 2, "pos", i);
            xml_add_int(line_xml, level + 2, "value", lines_gain[i] * freespins_mult);
            xml_add_int(line_xml, level + 2, "valueMoney", lines_gain[i] * freespins_mult * coin_value);
            xml_add_str(line_xml, level + 2, "hl", highlight[i]);
            xml_add_int(line_xml, level + 2, "symbolId", lines_gain_index[i]);
            xml_add_int(line_xml, level + 2, "multiplier", freespins_mult);
            xml_add_node(wlines_xml, level + 1, "line", line_xml);
        }
    }
    xml_add_node(destiny, level, "lines", wlines_xml);

    if (number_of_scatters > 1) {
        char scatters_xml[XMLMAX] = "";
        xml_add_int(scatters_xml, level + 1, "value", scatter_value[number_of_scatters-2] * number_of_coins * number_of_lines * freespins_mult);
        xml_add_int(scatters_xml, level + 1, "valueMoney", scatter_value[number_of_scatters-2] * number_of_coins * number_of_lines * freespins_mult * coin_value);
        xml_add_int(scatters_xml, level + 1, "multiplier", freespins_mult);
        xml_add_str(scatters_xml, level + 1, "hl", highlight_scatter);
        xml_add_node(destiny, level, "scatters", scatters_xml);
    }

    xml_add_int(destiny, level, "bonusRequest", ss_state == STATE_BEGIN ? 0 : (flag_bonus1_won || flag_bonus2_won));

    if (flag_bonus1_won || flag_bonus2_won) {
        char bonuses_xml[XMLMAX] = "";
        if (flag_bonus1_won) {
            char bonus_xml[XMLMAX] = "";
            xml_add_int(bonus_xml, level + 2, "id", 1);
            xml_add_str(bonus_xml, level + 2, "hl", highlight_bonus1);
            xml_add_node(bonuses_xml, level + 1, "bonus", bonus_xml);
        }
        if (flag_bonus2_won) {
            char bonus_xml[XMLMAX] = "";
            xml_add_int(bonus_xml, level + 2, "id", 2);
            xml_add_str(bonus_xml, level + 2, "hl", highlight_bonus2);
            xml_add_node(bonuses_xml, level + 1, "bonus", bonus_xml);
        }
        xml_add_node(destiny, level, "bonuses", bonuses_xml);
    }

    if (flag_freespins_on) {
        char freespins_xml[XMLMAX] = "";
        xml_add_int(freespins_xml, level + 1, "left", (freespins_total==freespins_left) ?freespins_left : (freespins_left+1));
        xml_add_int(freespins_xml, level + 1, "total", freespins_total);
        xml_add_int(freespins_xml, level + 1, "totalFreeGamesWinnings", freespins_gain);
        xml_add_int(freespins_xml, level + 1, "totalFreeGamesWinningsMoney", freespins_gain * coin_value);
        xml_add_node(lines_xml, level, "freeGames", freespins_xml);
    }
}

static void build_bonus2_xml(char* node_xml, const char* tag, int level) {
    if (flag_bonus2_won) {
      memset(node_xml, 0, XMLMAX);
      xml_add_int(node_xml, level + 1, "bonusValue", bonus_gain);
      xml_add_int(node_xml, level + 1, "bonusValueMoney", bonus_gain * coin_value);
      xml_add_str(node_xml, level + 1, "bonusGain", intarray_to_str(buf, bonus2_gain, nr_of_options2, "-"));
      xml_add_str(node_xml, level + 1, "bonusGainMoney", intarray_to_str(buf, bonus2_gain_money, nr_of_options2, "-"));
      xml_add_int(node_xml, level + 1, "totalBonusWinnings", bonus_gain);
      xml_add_int(node_xml, level + 1, "totalBonusWinningsMoney", bonus_gain * coin_value);
      xml_add_int(node_xml, level + 1, "bonusId", 2);
      xml_add_int(node_xml, level + 1, "totalWinnings", total_gain);
      xml_add_int(node_xml, level + 1, "totalWinningsMoney", total_gain * coin_value);
      xml_add_str(node_xml, level + 1, "choicesOrder", intarray_to_str(buf, bonus2_options, nr_of_options2, "-"));
      xml_add_str(node_xml, level + 1, "choicesWinnings", intarray_to_str(buf, bonus2_gain, nr_of_options2, "-"));
      xml_add_int(node_xml, level + 1, "bonusRequest", bonus_count < nr_of_choices2);
      xml_add_int(node_xml, level + 1, "bonusesLeft", 0);
      xml_add_tag(node_xml, level, tag);
    }
}

void send_data(int phase) {
  char xml_string[XMLMAX], xml_temp[XMLMAX], buf[XMLMAX], xml_temp1[XMLMAX], xml_string1[XMLMAX], lines_temp[XMLMAX], lines_xml[XMLMAX];
  memset(xml_string, 0, XMLMAX);
  memset(xml_temp, 0, XMLMAX);

        int bonus2_gain[BONUSOPTIONS], bonus2_gain_money[BONUSOPTIONS] bonus2_zeros[BONUSOPTIONS];
        for (i = 0; i < nr_of_options2; i++) {
            bonus2_zeros[i] = 0;
            bonus2_gain[i] = bonus2_winnings[i] * number_of_coins * number_of_lines;
            bonus2_gain_money[i] = bonus2_winnings[i] * number_of_coins * number_of_lines * coin_value;
        }

        int bonus1_choices_winnings[BONUSOPTIONS1];
        for (i = 0; i < nr_of_options1; i++) {
            bonus1_choices_winnings[i] = winnings_fs[i] == 0 ? bonus1_winnings[i] : bonus1_fs_won[i];
        }

  int i;
  switch (phase) {
    // (re)start of the game
    case STATE_INITIAL:
      memset(xml_string, 0, XMLMAX);
      xml_add_int(xml_string, 2, "coins", number_of_coins);
      xml_add_int(xml_string, 2, "coinValue", coin_value);
      xml_add_int(xml_string, 2, "lines", number_of_lines);
      xml_add_int(xml_string, 2, "money", player_money);
      xml_add_str(xml_string, 2, "currentState", ss_state == STATE_GAIN ? "interrupted" : client_states[ss_state]);

      // if in bonus or double/collect screen, regenerate the endGame xml
      if (ss_state != STATE_BONUS_ROUND && ss_state != STATE_END_GAME) {
        memset(xml_string, 0, XMLMAX);
        add_endgame_info(lines_xml, 4);
        xml_add_int(lines_xml, 4, "bet", number_of_coins * number_of_lines * coin_value);
        xml_add_int(lines_xml, 4, "totalWinnings", total_gain);
        xml_add_int(lines_xml, 4, "doubleRequest", (!flag_doq_on && total_gain && !flag_bonus1_won && !flag_bonus2_won && !flag_freespins_on && (doubles != 5)));
        xml_add_tag(lines_xml, 3, "endGame");

        if ((flag_bonus1_won || flag_bonus2_won) && ss_state != STATE_BONUS_CHOICE ) {
          if (flag_bonus1_won) {
            memset(xml_string1, 0, XMLMAX);
            xml_add_int(xml_string1, 4, "totalFreeGames", bonus_freespins);
            xml_add_int(xml_string1, 4, "bonusId", 1);
            xml_add_int(xml_string1, 4, "totalWinnings", total_gain);
            xml_add_int(xml_string1, 4, "totalWinningsMoney", total_gain * coin_value);
            xml_add_int(xml_string1, 4, "totalMultiplier", total_bonus / (number_of_coins * number_of_lines);
            xml_add_str(xml_string1, 4, "choicesOrder", intarray_to_str(buf, numbers_bonus1, nr_of_options1, "-"));
            xml_add_str(xml_string1, 4, "choicesWinnings", intarray_to_str(buf, bonus1_choices_winnings, nr_of_options1, "-"));
            xml_add_str(xml_string1, 4, "choicesMult", intarray_to_str(buf, winnings_mult, nr_of_options1, "-"));
            xml_add_str(xml_string1, 4, "choicesFreeGames", intarray_to_str(buf, winnings_fs, nr_of_options1, "-"));
            xml_add_int(xml_string1, 4, "bonusRequest", ((bonus_count<nr_of_choices1) ||(bonus1_1_gain!=0)) ? 1:0);
            xml_add_int(xml_string1, 4, "bonusesLeft", 0);
            xml_add_node(lines_xml, 3, "bonusWin", xml_string1);

            // add free spins 
            memset(xml_string1, 0, XMLMAX);
            xml_add_int(xml_string1, 4, "new", freespins);
            xml_add_int(xml_string1, 4, "total", freespins_total);
            xml_add_tag(xml_string1, 3, "freeGamesWin");
            xml_cat(lines_xml, xml_string1);
          } else if (flag_bonus2_won) {
            build_bonus2_xml(xml_string1, "bonusWin", 3);
            xml_cat(lines_xml, xml_string1);
          }
        }

        xml_add_node(xml_string, 2, "lastGame", lines_xml);
      }

      xml_add_node(data_buffer, 1, "initial", xml_string);
      break;
    // after the reels spin
    case STATE_END_GAME:
      memset(xml_string, 0, XMLMAX);
      add_endgame_info(xml_string, 2);
      
      // add freespins won during current game
      if (flag_freespins_won) {
        memset(xml_temp, 0, XMLMAX);
        xml_add_int(xml_temp, 3, "total", freespins);
        xml_add_node(xml_string, 2, "freeGamesWin", xml_temp);
      }  

      xml_add_int(xml_string, 2, "totalWinnings", total_gain);
      xml_add_int(xml_string, 2, "totalWinningsMoney", total_gain * coin_value);
      xml_add_int(xml_string, 2, "doubleRequest", (!flag_doq_on && total_gain && !flag_bonus1_won && !flag_bonus2_won && !flag_freespins_on));
      xml_add_node(data_buffer, 1, client_states[phase], xml_string);
      break;
    // when the player is in the client's double/collect screen and has not chosen yet to double or collect
    case STATE_DOUBLE_CHOICE:
      memset(xml_string, 0, XMLMAX);
      xml_add_int(xml_string, 2, "winningOption", doubles==0)? -1: last_colours[0]);
      xml_add_int(xml_string, 2, "currentPhase", doubles);
      xml_add_int(xml_string, 2, "totalPhaseCount", MAX_DOUBLE);
      xml_add_str(xml_string, 2, "previousCards", intarray_to_str(buf, last_colours, SAVED_COLOURS - 1, "|"));
      xml_add_int(xml_string, 2, "doubleFrom", total_gain);
      xml_add_int(xml_string, 2, "doubleFromMoney", total_gain * coin_value);
      xml_add_int(xml_string, 2, "doubleTo", total_gain * 2);
      xml_add_int(xml_string, 2, "doubleToMoney", total_gain * 2 * coin_value);
      xml_add_node(data_buffer, 1, client_states[ss_state], xml_string);
      break;
    // when the player has collected or reached maximum number of doubles and automatically collects
    case STATE_GAIN:
      memset(xml_string, 0, XMLMAX);
      xml_add_int(xml_string, 2, "totalWinnings", total_gain);
      xml_add_int(xml_string, 2, "totalWinningsMoney", total_gain * coin_value);
      xml_add_int(xml_string, 2, "money", player_money);
      xml_add_node(data_buffer, 1, client_states[phase], xml_string);
      break;
  case STATE_BONUS_CHOICE:
    if(flag_bonus1_won) {
      memset(xml_string, 0, XMLMAX);
      xml_add_str(xml_string, 2, "choicesOrder", intarray_to_str(buf, numbers_bonus1, nr_of_options1, "-"));
      xml_add_str(xml_string, 2, "choicesWinnings", intarray_to_str(buf, bonus1_choices_winnings, nr_of_options1, "-"));
      xml_add_str(xml_string, 2, "choicesMult", intarray_to_str(buf, winnings_mult, nr_of_options1, "-"));
      xml_add_str(xml_string, 2, "choicesFreeGames", intarray_to_str(buf, winnings_fs, nr_of_options1, "-"));
      xml_add_int(xml_string, 2, "bonusId", 1);
      xml_add_int(xml_string, 2, "totalBonusWinnings", total_bonus);
      xml_add_int(xml_string, 2, "totalBonusWinningsMoney", total_bonus * coin_value);
      xml_add_int(xml_string, 2, "totalMultiplier", total_bonus / (number_of_coins * number_of_lines));
    } else if (flag_bonus2_won) {
      memset(xml_string, 0, XMLMAX);
      xml_add_str(xml_string, 2, "bonusGain", intarray_to_str(buf, bonus2_gain, nr_of_options2, "-"));
      xml_add_str(xml_string, 2, "bonusGainMoney", intarray_to_str(buf, bonus2_gain_money, nr_of_options2, "-"));
      xml_add_str(xml_string, 2, "choicesOrder", intarray_to_str(buf, bonus2_zeros, nr_of_options2, "-"));
      xml_add_str(xml_string, 2, "choicesWinnings", intarray_to_str(buf, bonus2_zeros, nr_of_options2, "-"));
      xml_add_int(xml_string, 2, "bonusId", 2);
      xml_add_int(xml_string, 2, "totalBonusWinnings", bonus_gain);
      xml_add_int(xml_string, 2, "totalBonusWinningsMoney", bonus_gain * coin_value);
    }
    xml_add_node(data_buffer, 1, client_states[ss_state], xml_string);
    break;
  case STATE_BONUS_WIN:
    if(flag_bonus1_won) {
      memset(xml_string, 0, XMLMAX);
      //multiplier bonus win
      if (bonus_type == 1) {
        xml_add_int(xml_string, 2, "bonusTotalFree", bonus_freespins);
        xml_add_int(xml_string, 2, "totalBonusWinnings", total_bonus);
        xml_add_int(xml_string, 2, "totalBonusWinningsMoney", total_bonus * coin_value);
        xml_add_int(xml_string, 2, "totalMultiplier", total_bonus / (number_of_coins * number_of_lines));
      } else { //freespins won 
        xml_add_int(xml_string, 2, "totalFreeGames", bonus_freespins);
        xml_add_int(xml_string, 2, "totalBonusWinnings", total_bonus);
        xml_add_int(xml_string, 2, "totalBonusWinningsMoney", total_bonus * coin_value);
        xml_add_int(xml_string, 2, "totalMultiplier", total_bonus / (number_of_coins * number_of_lines));
      }
      xml_add_int(xml_string, 2, "bonusId", 1);
      xml_add_str(xml_string, 2, "choicesOrder", intarray_to_str(buf, numbers_bonus1, nr_of_options1, "-"));
      xml_add_str(xml_string, 2, "choicesWinnings", intarray_to_str(buf, bonus1_choices_winnings, nr_of_options1, "-"));
      xml_add_str(xml_string, 2, "choicesMult", intarray_to_str(buf, winnings_mult, nr_of_options1, "-"));
      xml_add_str(xml_string, 2, "choicesFreeGames", intarray_to_str(buf, winnings_fs, nr_of_options1, "-"));
      xml_add_int(xml_string, 2, "bonusRequest", 0);
      xml_add_int(xml_string, 2, "bonusesLeft", bonus1_count < nr_of_choices1 - 1);
      xml_add_int(xml_string, 2, "totalWinnings", total_gain);
      xml_add_int(xml_string, 2, "totalWinningsMoney", total_gain * coin_value);
      // add freespins won during current game
      if(ready && flag_freespins_won) {
          memset(xml_temp, 0, XMLMAX);
          xml_add_int(xml_temp, 3, "total", freespins);
          xml_add_node(xml_string, 2, "freeGamesWin", xml_temp);
      }

      xml_add_int(xml_string, 2, "money", player_money);
      xml_add_tag(xml_string, 1, client_states[phase]);

      if (flag_freespins_on && freespins_left == 0) {
        memset(xml_temp, 0, XMLMAX);
        xml_add_int(xml_temp, 2, "left", freespins_left);
        xml_add_int(xml_temp, 2, "total", freespins_total);
        xml_add_int(xml_temp, 2, "totalFreeGamesWinnings", freespins_gain);
        xml_add_int(xml_temp, 2, "totalFreeGamesWinningsMoney", freespins_gain * coin_value);
        xml_add_node(xml_string, 1, "freeGames", xml_temp);
      }
      // add to big buffer
      xml_cat(data_buffer, xml_string);
      break;
    } else if (flag_bonus2_won) {
      build_bonus2_xml(xml_string, client_states[phase], 1);
      xml_cat(data_buffer, xml_string);
      break;
    }
    // send update money
    case STATE_MONEY_UPDATE:
      memset(xml_string, 0, XMLMAX);
      xml_add_int(xml_string, 2, "money", player_money);
      xml_add_node(data_buffer, 1, client_states[phase], xml_string);
      break;
  }
}

/* ==================================================================================================
    READ BET FROM CLIENT
================================================================================================== */
void bet() {
  int cmd1, cmd2;
  char buf[XMLMAX];

  init_vars();  // reinitialize variables
  // exit game at new bet, if game blocked
  if (game_blocked && !flag_freespins_on) {
    memset(buf, 0, XMLMAX);
    xml_add_str(buf, 0, "error", ERROR_BLOCK);
    awrite(0, buf);
    memset(screen, 0, MAX);
    sprintf(screen, "game blocked");
    write_nmise(ERROR);
    exit_nmise(1);  // normal exit
  }
  cmd1 = read_cmd(0);
  cmd2 = read_cmd(0);
  if (!flag_freespins_on) {
    // checks if bet per line is in the defined range
    if (cmd1 < 1 || cmd1 > BETS)
      general_error(ERROR_INVALID_VALUE, "invalid bet per line read from client");
    // checks if number of lines is in the defined range
    if (cmd2 < 1 || cmd2 > LINES)
      general_error(ERROR_INVALID_VALUE, "invalid number of lines read from client");
    // checks if the player has got enough money
    player_money = get_player_money();
    if (cmd1 * cmd2 * coin_value - player_money > 0.001)
      general_error(ERROR_NO_MONEY, "-");
    number_of_coins = cmd1;
    number_of_lines = cmd2;
    // bet is fine, proceed
    update_player_money(-number_of_coins * number_of_lines * coin_value);  // extract money
    player_money -= number_of_coins * number_of_lines * coin_value;
    write_nmise(BET);                      // mark it in statistics
  }
}

/* ==================================================================================================
    WILD PLACEMENT
================================================================================================== */
void place_wild() {
  int line_pos;
  int col_pos;
  int wild_roll;

  /* 1 chance for each reel */
  for (col_pos = 0; col_pos < COLS; col_pos++) {
    wild_roll = genrand_range(0, 999);
    if (wild_roll < wild_odds[flag_freespins_on][col_pos]) {
      /* Determine which row randomly */
      line_pos = genrand_range(0, 2);
      combination[line_pos][col_pos] = WILD_SYMB;
    }
  }
}

/* ==================================================================================================
    SCATTER/BONUS1/BONUS2 SYMBOLS PLACEMENT
================================================================================================== */
void place_special() {
  int line_pos;
  int col_pos;
  int special_roll;
  int i;

  for (col_pos = 0; col_pos < COLS; col_pos++) {
    special_roll = genrand_range(0, 999);
    for (i = 0; i < NB_SPECIAL_SYMB; i++) {
      if (special_roll < special_odds[flag_freespins_on][i][col_pos]) {
        /* Determine which row randomly */
        line_pos = genrand_range(0, 2);
        /* Wilds have priority on other specials */
        if (combination[line_pos][col_pos] != WILD_SYMB)
          combination[line_pos][col_pos] = WILD_SYMB + i + 1;
        break;
      }
    }
  }
}

/* ==================================================================================================
    CALCULATE THE GAIN
================================================================================================== */
int xinarow(int x, int pos, int l) {
  int count[17];
  int i;

  for (i=0; i<17; i++)
    count[i] = 0;
  for (i=pos; i<pos+x; i++)
    count[combination[lines[l][i]][i]]++;
  if (count[13] == x) {  // line of wilds
    current_index = 13;
    return gain_table[12][x-1] * number_of_coins;
  }
  for (i=12; i>=0; i--) {
    if ((count[i] + count[13] == x) && (gain_table[i-1][x-1] > 0)){
      current_index = i;
      return gain_table[i-1][x-1] * number_of_coins;
    }
  }
  return 0;
}

int score() {
  int i, j=0, res, restmp = 0, resmax = 0, indmax = -1, max = 0;
  res=0;

  //score
  for (i=0; i<number_of_lines; i++) {
    resmax = 0;
    for (j=5; j>=1; j--) {
      restmp = xinarow(j, 0, i);
      if (restmp > resmax) {
        resmax = restmp;
        indmax = current_index;
        max = j;
      }
    }
    if (resmax) {
      lines_gain[i] = resmax;
      lines_gain_index[i] = indmax +1  + 13*(max - 1);
      res += resmax;
      for (j=0; j<max; j++)
        highlight[i][j] = (char)((int)'1' + lines[i][j]);
    }
  }
  for (i=0; i<ROWS; i++)
    for (j=0; j<COLS; j++) {
      if (combination[i][j] == 14)
        number_of_scatters++;
      if (combination[i][j] == 15)
        number_of_bonuses1++;
      if (combination[i][j] == 16)
        number_of_bonuses2++;
    }

  // scatters
  if (number_of_scatters > 1) {
    res += scatter_value[number_of_scatters-2] * number_of_coins * number_of_lines;
    for (i=0; i<ROWS; i++)
      for (j=0; j<COLS; j++)
        if (combination[i][j] == 14)
          highlight_scatter[i*COLS+j] = '1';
  }

  //bonus1
  if (number_of_bonuses1 >= 3 && number_of_bonuses1 <= 5) {
    flag_bonus1_won =1;
    flag_bonus_won = 1;
    for (i=0; i<ROWS; i++)
      for (j=0; j<COLS; j++)
    if (combination[i][j] == 15)
      highlight_bonus1[i*COLS+j] = '1';
  }

  //bonus2
  if (number_of_bonuses2 >= 3 && number_of_bonuses2 <= 5) {
    flag_bonus2_won = 1;
    flag_bonus_won = 1;
    for (i=0; i<ROWS; i++)
      for (j=0; j<COLS; j++)
    if (combination[i][j] == 16)
      highlight_bonus2[i*COLS+j] = '1';
  }

  partial_gain = res;
  if (flag_freespins_on) {
    res *= freespins_mult;
  }

  return res;
}

/* ==================================================================================================
    PLAYER WINS
================================================================================================== */
void win(int send) {
/*  int tmp = 0;
  if(ss_state == STATE_BONUS_ROUND)
    tmp = 1;*/
  ss_state = STATE_GAIN;
  update_player_money(total_gain * coin_value);  // add money
  player_money += total_gain * coin_value;
  ss_state = STATE_UPDATE_GAIN;
  player_money = get_player_money();             // refresh the player money (might be different than what's expected if the player plays something in parallel
  // mark it in the statistics
  if (flag_bonus1_won ||  flag_bonus2_won || flag_freespins_on)
    write_nmise(WON);                            // player has won with bonus (autocollect, no doubles)
  else if (limit)
    write_nmise(WON_AND_LIMIT);                  // player has won 5 consecutive doubles
  else
    write_nmise(WON_AND_STOP);                   // player has chosen to collect
  // end the phase - for aams version only
  ss_state = STATE_END_PHASE;
  end_phase();
  ss_state = STATE_COPY_PHASE;
  copy_phase();
  if (send) {                                    // send gain to client only if specificed by the argument
    if (flag_bonus1_won || flag_bonus2_won)
      send_data(STATE_BONUS_WIN);
    send_data(STATE_GAIN);
  }
  ss_state = STATE_BEGIN;
  test_client = 0;
  update_freespins();
}

/* ==================================================================================================
    PLAYER LOSES
================================================================================================== */
void lose(int send) {
  ss_state = STATE_GAIN;
  write_nmise(LOST);
  // end the phase - for aams version only
  ss_state = STATE_END_PHASE;
  end_phase();
  ss_state = STATE_COPY_PHASE;
  copy_phase();
  // send gain to client only if specificed by the argument
  if (send)
    send_data(STATE_GAIN);
  // new phase can start
  ss_state = STATE_BEGIN;
  test_client = 0;
  update_freespins();
}

/* ==================================================================================================
    SPIN THE REELS
================================================================================================== */
void get_result_astro() {
int i, j, rpos, t;
char buf[MAX];
  t = flag_freespins_on;
  memset(reel_pos, 0, 50);
  memset(screen, 0, MAX);
  for (i=0; i<COLS; i++) {
    // spin the reels and read
    rpos = genrand_range(0,max_reel[t][i]-1);
    for (j=0; j<ROWS; j++)
      // "spin" to get the combination
      combination[j][i] = reels[t][i][(rpos+j)%max_reel[t][i]];
    memset(buf, 0, MAX);
    sprintf(buf, "%d", rpos);  // save position for statistics
    strcat(reel_pos, buf);
    memset(buf, 0, MAX);
    sprintf(buf, "%d:%d:%d", combination[0][i], combination[1][i], combination[2][i]);
    strcat(screen, buf);
    if (i < COLS-1) {
      strcat(reel_pos, "-");
      strcat(screen, "-");
    }
  }
}

/* ==================================================================================================
    SPIN THE REELS
================================================================================================== */
void spin() {
  if (test_client)
    set_test();
  else {
    get_result_astro();
    place_wild();
    place_special();
  }
  total_gain = score();
  /* We add partial gain only if FS and no bonus won (if bonus, it will be added there) */
  if(flag_freespins_on && !flag_bonus_won)
    freespins_gain += partial_gain;
  if (flag_bonus1_won || flag_bonus2_won)
    ss_state = STATE_BONUS_ROUND;
  else
    ss_state = STATE_END_GAME;
  send_data(STATE_END_GAME);
  if (!total_gain && !flag_bonus1_won && !flag_bonus2_won)
    lose(1);
  else if (total_gain && !flag_bonus1_won && !flag_bonus2_won) {
    if (flag_doq_on) {
      write_nmise(WON_AND_CHOICE);
      memset(screen, 0, MAX);
      strcpy(screen, "-");
    }
    else
      win(1);
  }
}
