#include <isbc/log.h>
#include "process_game.h"
#include "bonus_round.h"
#include "db_interface.h"
#include "double_gain.h"
#include "init.h"
#include "mechanics.h"
#include "slt_9l_astro.h"
#include "../../headers/values_error_codes.h"

/******************************************
      DEFINES
******************************************/


/******************************************
      STATIC FONCTIONS
******************************************/





/******************************************
      FUNCTIONS
******************************************/

/**
 * \brief Executes the game process.
 * \param game game values
 * \param configuration config values
 * \return 0 if OK
 */
int processGamePlay(Game* game, Config* configuration)
{
    int loopExit = 1;

    // write the last xml chunk to the client
    if (strcmp(data_buffer, "") != 0) {
        memset(data_buffer_temp, 0, XMLMAX * 5);
        add_to_xml(data_buffer_temp, 1, 0, "data", data_buffer);
        awrite(0, data_buffer_temp);
        memset(data_buffer, 0, XMLMAX * 5);
    }

    while (loopExit) {

    // read command from client
    cmd = read_cmd(0);

    // execute!
    switch (cmd) {
        // player changes coin value
        case CMD_COIN:
            // command only available in 2 states
            // if here from STATE_END_GAME, no doubling is allowed after the coin value change
            // auto-proceed with the collect
            if (ss_state == STATE_END_GAME && !flag_server_paused)
                win(1);    // sends result, adds money, records accounting data, and sets state to STATE_BEGIN
            if (ss_state == STATE_BEGIN && !flag_server_paused)
                coin_value = set_coin_value();
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // player bets (starts a new game)
        case CMD_BET:
            // command only available in 2 states
            // if here from STATE_END_GAME, no doubling is allowed after the coin value change
            // auto-proceed with the collect
            if (ss_state == STATE_END_GAME && !flag_server_paused)
                win(1);    // sends result, adds money, records accounting data, and sets state to STATE_BEGIN
            if (ss_state == STATE_BEGIN && !flag_server_paused) {
                bet();     // reads bet, extracts money, records accounting data
                spin();    // spins the reels
            }
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // player enters bonus1 screen
        case CMD_ENTER_BONUS:
            // command only available in 1 state
            if (ss_state == STATE_BONUS_ROUND && !flag_server_paused) {
                if (flag_bonus1_won)
                    bonus1_choice();
                else 
                    bonus2_choice();
                // unimportant to the server, this only marks a new screen mode in the client for savescreen
                ss_state = STATE_BONUS_CHOICE;
                send_data(ss_state);
            }
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // client sends a bonus value/choice to the server
        case CMD_BONUS:
            // command only available in 1 state
            if (ss_state == STATE_BONUS_CHOICE && !flag_server_paused) {
                if (flag_bonus1_won) 
                    bonus1_round();
                else
                    bonus2_round();
            }
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // player enters the double/collect  screen after a gain (without automatically doubling)
        case CMD_ENTER_DOUBLE:
            // command only available in 1 state
            if (ss_state == STATE_END_GAME && !flag_server_paused) {
                // unimportant to the server, this only marks a new screen mode in the client for savescreen
                ss_state = STATE_DOUBLE_CHOICE;
                send_data(ss_state);
            }
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // player chooses to collect his gain
        case CMD_COLLECT:
            // command only available in 2 states
            if ((ss_state == STATE_DOUBLE_CHOICE || ss_state == STATE_END_GAME) && !flag_server_paused)
                win(1);    // sends result, adds money, records accounting data, and sets state to STATE_BEGIN
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // player chooses to try to double his gain
        case CMD_DOUBLE:
            // command only available in 1 state
            if (ss_state == STATE_DOUBLE_CHOICE && !flag_server_paused)
                double_gain();
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // client sends pause command
        case CMD_PAUSE:
            flag_server_paused = flag_server_paused ? 0 : 1;
            // if unpaused, there should be a money update
            if (!flag_server_paused) {
                player_money = get_player_money();
                send_data(STATE_MONEY_UPDATE);
            }
            break;
        // player logs out
        case CMD_LOGOUT:
            if (!flag_server_paused ) {
                write_nmise(LOGOUT);
                end_session("", "");  // end the session - for aams version only
                exit_nmise(1);  // normal exit
            }
        // client sends the request for strips
        case CMD_DOQ_ENABLE:
            // command only available in 1 state
            if (ss_state == STATE_BEGIN)
                flag_doq_on = 1;
            // if sent at a bad moment (corrupt/bugged client): exit game
            else
                general_error(ERROR_WRONG_COMMAND, "command not allowed at this point");
            break;
        // client sends invalid command (corrupt/bugged client): exit game
        default:
            general_error(ERROR_INVALID_COMMAND, "invalid command read from client");
    }
  }
    return PROCESS_OK;
}
