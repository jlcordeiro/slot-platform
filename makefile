GIMOLIBS_LIB = /usr/local/gimolib/
GIMOLIBS_HDR = /usr/local/gimolib/include/

GCC=clang
CFLAGS+= -DINI_PATH=$(INI_PATH)
#CFLAGS+= -Wall -Wconversion -Wno-self-assign -Wextra -pedantic -D_POSIX_SOURCE -D_GNU_SOURCE -std=c99 -Wno-gnu
CFLAGS+= -D_POSIX_SOURCE -D_GNU_SOURCE -std=c99 -Wno-gnu
CFLAGS+= -Wno-write-strings
CFLAGS+= -O0 -ggdb -pg

IPATH+= -I/usr/local/gimolib/include/ -I/usr/include/mysql
LDFLAGS+= -L/usr/lib/mysql -L/usr/local/gimolib/ -Wl,-rpath=/usr/local/gimolib/ -lmysqlclient_r -lpthread -liface -lmtgen -lz

OBJECT_FILES = bonus_round.o db_interface.o double_gain.o init.o mechanics.o process_login.o process_game.o

all: astro

cheat: GCC += -DCHEATING_MODE
cheat: astro

mechanics:
	$(GCC) $(CFLAGS) $(IPATH) mechanics.c -c

process_login:
	$(GCC) $(CFLAGS) $(IPATH) -DGAME_ID=$(GAME_ID) process_login.c -c

process_game:
	$(GCC) $(CFLAGS) $(IPATH) process_game.c -c

bonus_round:
	$(GCC) $(CFLAGS) $(IPATH) bonus_round.c -c

db_interface:
	$(GCC) $(CFLAGS) $(IPATH) db_interface.c -c

double_gain:
	$(GCC) $(CFLAGS) $(IPATH) double_gain.c -c

init:
	$(GCC) $(CFLAGS) $(IPATH) init.c -c

astro: bonus_round db_interface double_gain init mechanics process_login process_game
	$(GCC) $(CFLAGS) $(IPATH) main.c $(OBJECT_FILES) -o dice $(LDFLAGS)

clean:
	$(RM) *.o astro
