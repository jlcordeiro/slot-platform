/* =================================================================================================
		READS & SETS  VALUES FOR TEST MODE
================================================================================================== */

int test_combination[ROWS][COLS];	// combination to test
int test_client = 0;
int test_bonus = 0;
char strips_name[STRIPS][8] = {"normal"};


// reads cards for test from client
void read_test() {
char *t1;
int i, j;
char cmd[MAX];
	for (i=0; i < ROWS; i++)
		for (j=0; j<COLS; j++)
			test_combination[i][j] = -1;

	if (strncmp(TESTUSER, login_string, strlen(TESTUSER)) == 0) { // test user
		aread(0, cmd);
		j = 0;
		for (t1 = strtok(cmd, ","); t1 != NULL; t1 = strtok(NULL, ",")) {
			test_combination[j/COLS][j%COLS] = atoi(t1);
			if (atoi(t1) > MAX_SYMBOL_VALUE || atoi(t1) < MIN_SYMBOL_VALUE)
				general_error(ERROR_INVALID_VALUE, "invalid symbol read from client");
			j++;
		}
		test_client = 1;
	}
	else
		general_error(ERROR_BAD_LOGIN, "user not allowed for test");
}


// sets first hand for test
void set_test(){
int i, j;
char buf[MAX];
	for (i=0; i<ROWS; i++) 
		for (j=0; j<COLS; j++)
			combination[i][j] = test_combination[i][j];
	memset(buf, 0, MAX);
	for (i=0; i<COLS; i++) {
		sprintf(buf, "%d:%d:%d", test_combination[0][i], test_combination[1][i], test_combination[2][i]);
		strcat(screen, buf);
		if (i < COLS-1) {
			strcat(reel_pos, "-");    
			strcat(screen, "-");
		}	
	}			
}


void send_strips() {
char xml_string[2*XMLMAX], xml_temp[2*XMLMAX], buf[2*XMLMAX], reel_temp[2*XMLMAX], strip_temp[2*XMLMAX], many_reels[2*XMLMAX];
int i,j,k;

    if (strncmp(TESTUSER, login_string, strlen(TESTUSER)) == 0) { // test user
    memset(xml_string, 0,2*XMLMAX);
           memset(strip_temp, 0, 2*XMLMAX);
    for (k=0;k<STRIPS;k++){
            memset(reel_temp, 0, 2*XMLMAX);
            memset(many_reels, 0, 2*XMLMAX);
            memset(buf, 0, XMLMAX);
            add_to_xml(buf, 0, 3, "name", strips_name[k]);
            strcat(many_reels, buf);
            for (j=0; j<COLS; j++){
                memset(xml_temp, 0, XMLMAX);
                sprintf(xml_temp, "%d", reels[k][j][0]);
                for (i=1; i<max_reel[k][j]; i++)
                   sprintf(xml_temp, "%s|%d", xml_temp, reels[k][j][i]);

                memset(buf, 0, XMLMAX);
                add_to_xml(buf, 0, 4, "reel", xml_temp);
                strcat(reel_temp, buf);
            }
            // add all inside specific tag
            memset(buf, 0, 2*XMLMAX);
            add_to_xml(buf, 1, 3, "reels", reel_temp);
            strcat(many_reels, buf);
            // add all inside specific tag
            memset(buf, 0, 2*XMLMAX);
            add_to_xml(buf, 1, 2, "strip", many_reels);
            strcat(strip_temp, buf);
        }

    memset(buf, 0, 2*XMLMAX);
    add_to_xml(buf, 1, 1, "strips", strip_temp);
    strcat(xml_string, buf);
    strcat(data_buffer, xml_string);
    }
    else
        general_error(ERROR_BAD_LOGIN, "user not allowed for test");
}

