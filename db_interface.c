#include <isbc/log.h>
#include "db_interface.h"

/******************************************
      DEFINES
******************************************/



/******************************************
      STATIC FONCTIONS
******************************************/





/******************************************
      FUNCTIONS
******************************************/

/**
 * \brief Executes the game process.
 * \param game game values
 * \param configuration config values
 * \return 0 if OK
 */


/* ==================================================================================================
    WRITE STATISTICS
===================================================================================================*/
void write_nmise(long long action) {
  int date;
  // write this game's specific variables to the statistics
  date = time(NULL);
  nmise(player_money, action, date, flag_freespins_on, freespins_left, freespins_mult, flag_bonus_won, 0, 0 , nmise_action_index, 0, 0, action == BET ? number_of_coins * number_of_lines * coin_value : 0, (action== WON)?total_gain * coin_value : 0, coin_value, screen, number_of_coins,reel_pos, action == ERROR ? screen_error : "", 0);
  send_id(0);
  nmise_action_index++;
}

/* ==================================================================================================
    SAVESCREEN: RESTORE LAST GAME'S VARIABLES
================================================================================================== */
void ss_restore_parameters() {
  char **buf;
  int n, i, j, total;
  // nothing there, so nothing to restore
  if (strcmp(ss_parameters, "-") == 0)
    return;
  // otherwise, break it into peaces
  buf = my_explode("|", ss_parameters, &n);
  // and restore
  // all general game variables
  total = 0;
  coin_value = atoi(buf[0]);
  number_of_coins = atoi(buf[1]);
  number_of_lines = atoi(buf[2]);
  total += 3;
  for (i=0; i<ROWS; i++)
    for (j=0; j<COLS; j++)
      combination[i][j] = atoi(buf[total+i*COLS+j]);
  total += (ROWS * COLS);
  for (i=0; i<LINES; i++)
    lines_gain[i] = atoi(buf[total+i]);
  total += LINES;
  for (i=0; i<LINES; i++)
    lines_gain_index[i] = atoi(buf[total+i]);
  total += LINES;
  for (i=0; i<SAVED_COLOURS; i++)
    last_colours[i] = atoi(buf[total+i]);
  total += SAVED_COLOURS;
  for (i=0; i<BONUSOPTIONS; i++)
    bonus2_options[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS;
  for (i=0; i<BONUSOPTIONS; i++)
    bonus2_winnings[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS;
  for (i=0; i<BONUSOPTIONS; i++)
    bonus_parts_values[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS;
  for (i=0; i<BONUSOPTIONS1; i++)
    numbers_bonus1[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS1;
  for (i=0; i<BONUSOPTIONS1; i++)
    bonus1_winnings[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS1;
  for (i=0; i<BONUSOPTIONS1; i++)
    winnings_mult[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS1;
  for (i=0; i<BONUSOPTIONS1; i++)
    winnings_fs[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS1;
  for (i=0; i<BONUSOPTIONS1; i++)
    bonus1_fs_won[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS1;
  for (i=0; i<BONUSOPTIONS1; i++)
    bonus1_options[i] = atoi(buf[total+i]);
  total += BONUSOPTIONS1;
  number_of_scatters = atoi(buf[total]);
  total += 1;
  number_of_bonuses1 = atoi(buf[total]);
  total += 1;
  number_of_bonuses2 = atoi(buf[total]);
  total += 1;
  partial_gain = atoi(buf[total]);
  total += 1;
  bonus1[0] = atoi(buf[total]);
  total += 1;
  bonus1[1] = atoi(buf[total]);
  total += 1;
  bonus1[2] = atoi(buf[total]);
  total += 1;
  for(i=0;i<10;i++)
    bonus_1[i] = atoi(buf[total+i]);
  total += 10;
  for(i=0;i<5;i++)
    bonus1cmd[i] = atoi(buf[total+i]);
  total += 5;
  bonus1_1_gain = atoi(buf[total]);
  total += 1;
  bonus1_2_gain = atoi(buf[total]);
  total += 1;
  bonus1_3_gain = atoi(buf[total]);
  total += 1;
  for(i=0;i<12;i++)
    bonus2_gain[i] = atoi(buf[total]);
  total += 12;
  total_gain = atoi(buf[total]);
  total += 1;
  total_bonus = atoi(buf[total]);
  total += 1;
  total_bonus2 = atoi(buf[total]);
  total += 1;
  flag_bonus_won = atoi(buf[total]);
  total += 1;
  nr_of_options2 = atoi(buf[total]);
  total += 1;
  nr_of_options1 = atoi(buf[total]);
  total += 1;
  nr_of_choices2 = atoi(buf[total]);
  total += 1;
  nr_of_choices1 = atoi(buf[total]);
  total += 1;
  show_all_bonus_flag = atoi(buf[total]);
  total += 1;
  bonus_count = atoi(buf[total]);
  total += 1;
  bonus1_count = atoi(buf[total]);
  total += 1;
  ready = atoi(buf[total]);
  total += 1;
  flag_bonus1_won = atoi(buf[total]);
  total += 1;
  flag_bonus2_won = atoi(buf[total]);
  total += 1;
  flag_freespins_won = atoi(buf[total]);
  total += 1;
  flag_freespins_on = atoi(buf[total]);
  total += 1;
  freespins_left = atoi(buf[total]);
  total += 1;
  bonus_freespins = atoi(buf[total]);
  total += 1;
  freespins_mult = atoi(buf[total]);
  total += 1;
  freespins = atoi(buf[total]);
  total += 1;
  freespins_total = atoi(buf[total]);
  total += 1;
  freespins_gain = atoi(buf[total]);
  total += 1;
  doubles = atoi(buf[total]);
  total += 1;
  bonuses1 = atoi(buf[total]);
  total += 1;
  limit = atoi(buf[total]);
  total += 1;
  for (i=0; i<LINES; i++)
    strcpy(highlight[i], buf[total+i]);
  total += LINES;
  strcpy(highlight_scatter, buf[total]);
  total += 1;
  strcpy(highlight_bonus1, buf[total]);
  total += 1;
  strcpy(highlight_bonus2, buf[total]);
  total += 1;
  strcpy(reel_pos, buf[total]);
  total += 1;
  // restore screen (for statistics)
  strcpy(screen, buf[total]);
}

/* ==================================================================================================
    sAVESCREEN: STORE CURRENT GAME'S VARIABLES
================================================================================================== */
void ss_store_parameters() {
  char buf[MAX];
  int i, j;
  memset(ss_parameters, 0, SSMAX);
  // insert all generic game variables, except player_money which should always be refreshed:
  sprintf(ss_parameters, "%d|%d|%d|", coin_value, number_of_coins, number_of_lines);
  for (i=0; i<ROWS; i++)
    for (j=0; j<COLS; j++) {
      memset(buf, 0, MAX);
      sprintf(buf, "%d|", combination[i][j]);
      strcat(ss_parameters, buf);
    }
  for (i=0; i<LINES; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", lines_gain[i]);
    strcat(ss_parameters, buf);
  }
  for (i=0; i<LINES; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", lines_gain_index[i]);
    strcat(ss_parameters, buf);
  }
  for (i=0; i<SAVED_COLOURS; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", last_colours[i]);
    strcat(ss_parameters, buf);
  }
  for (i=0; i<BONUSOPTIONS; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus2_options[i]);
    strcat(ss_parameters, buf);
  }
  for (i=0; i<BONUSOPTIONS; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus2_winnings[i]);
    strcat(ss_parameters, buf);
  }
  for (i=0; i<BONUSOPTIONS; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus_parts_values[i]);
    strcat(ss_parameters, buf);          
  }
  for (i=0; i<BONUSOPTIONS1; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", numbers_bonus1[i]);
    strcat(ss_parameters, buf);          
  }
  for (i=0; i<BONUSOPTIONS1; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus1_winnings[i]);
    strcat(ss_parameters, buf);          
  }
  for (i=0; i<BONUSOPTIONS1; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", winnings_mult[i]);
    strcat(ss_parameters, buf);          
  }
  for (i=0; i<BONUSOPTIONS1; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", winnings_fs[i]);
    strcat(ss_parameters, buf);          
  }
  for (i=0; i<BONUSOPTIONS1; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus1_fs_won[i]);
    strcat(ss_parameters, buf);          
  }
  for (i=0; i<BONUSOPTIONS1; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus1_options[i]);
    strcat(ss_parameters, buf);          
  }
  memset(buf, 0, MAX);
  sprintf(buf, "%d|%d|%d|%d|%d|%d|%d|", number_of_scatters, number_of_bonuses1, number_of_bonuses2, partial_gain, bonus1[0], bonus1[1], bonus1[2]);
  strcat(ss_parameters, buf);
  for (i=0; i<10; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus_1[i]);
    strcat(ss_parameters, buf);
  }
  for (i=0; i<5; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus1cmd[i]);
    strcat(ss_parameters, buf);
  }
  memset(buf, 0, MAX);
  sprintf(buf, "%d|", bonus1_1_gain);
  strcat(ss_parameters, buf);
  memset(buf, 0, MAX);
  sprintf(buf, "%d|", bonus1_2_gain);
  strcat(ss_parameters, buf);
  memset(buf, 0, MAX);
  sprintf(buf, "%d|", bonus1_3_gain);
  strcat(ss_parameters, buf);
  for (i=0; i<12; i++) {
    memset(buf, 0, MAX);
    sprintf(buf, "%d|", bonus2_gain[i]);
    strcat(ss_parameters, buf);
  }
  memset(buf, 0, MAX);
  sprintf(buf, "%d|", total_gain);
  strcat(ss_parameters, buf);
  memset(buf, 0, MAX);
  sprintf(buf, "%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|",total_bonus,total_bonus2,flag_bonus_won,nr_of_options2, nr_of_options1, nr_of_choices2, nr_of_choices1, show_all_bonus_flag,bonus_count,bonus1_count,ready, flag_bonus1_won, flag_bonus2_won, flag_freespins_won, flag_freespins_on, freespins_left,bonus_freespins, freespins_mult, freespins, freespins_total, freespins_gain, doubles,bonuses1,limit);
  strcat(ss_parameters, buf);
  for (i=0; i<LINES; i++) {
    strcat(ss_parameters, highlight[i]);
    strcat(ss_parameters, "|");
  }
  strcat(ss_parameters, highlight_scatter);
  strcat(ss_parameters, "|");
  strcat(ss_parameters, highlight_bonus1);
  strcat(ss_parameters, "|");
  strcat(ss_parameters, highlight_bonus2);
  strcat(ss_parameters, "|");
  strcat(ss_parameters, reel_pos);
  strcat(ss_parameters, "|");
  // insert screen (for statistics)
  strcat(ss_parameters, screen);
}

/* ==================================================================================================
    sAVESCREEN: DO WHAT'S LEFT BEFORE WAITING FOR NEW COMMAND
================================================================================================== */
void ss_recovery() {
  memset(data_buffer, 0, XMLMAX * 5);
  send_id(1);
  send_data(STATE_INITIAL);
  switch (ss_state) {
    // exit before choosing double/collect, resend final hand
    case STATE_END_GAME:
      send_data(ss_state);
      break;
    case STATE_BONUS_ROUND:
      send_data(STATE_END_GAME);
      break;
    // exit while waiting for client to choose a bonus option
    case STATE_BONUS_CHOICE:
      send_data(ss_state);
      break;
    // exit while waiting for client to double/collect from the double/collect client screen
    case STATE_DOUBLE_CHOICE:
      send_data(ss_state);
      break;
    // database problem while adding money to client's account (result already generated)
    // retry the win function        
    case STATE_GAIN:
      win(1);
      break;
    // database problem while recording gain in the accounting table
    // retry and set the state for a new game
    case STATE_UPDATE_GAIN:
      player_money = get_player_money();        // refresh the player money (might be different than what's expected if the player plays something in parallel
      // mark it in the statistics
      write_nmise(WON);
      // end the phase - for aams version only
      ss_state = STATE_END_PHASE;
    case STATE_END_PHASE:
      end_phase();
      ss_state = STATE_COPY_PHASE;
    case STATE_COPY_PHASE:
      copy_phase();
      send_data(STATE_GAIN);
      // new phase can start
      ss_state = STATE_BEGIN;
      break;
    default:
      break;
  } //end switch
}
