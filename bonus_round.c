#include "bonus_round.h"

/* ==================================================================================================
    BONUSES GENERATOR
================================================================================================== */
/* Bonus 1 management */
int bonus1_generate() { //multiplier
  int tmp, pos, i, frees_won;
  bonus1_gain = 0;
  for (i = 0; i < number_of_bonuses1; i++) {
    /* Check what type of bonus */
    tmp = genrand_range(0, BONUS1_TYPES - 1);
    /* There can be FS only on paid games */
    if (flag_freespins_on == 1)
      tmp = 1;
    if (tmp == 0)
    {
      /* Trigger Free Spin */
      tmp = genrand_range(0, 99);
      pos = 0;
      do {
        if (tmp < bonus1fs[pos])
          break;
        pos++;
      }while(pos < BONUS1_FS_RANGE);
      flag_freespins_won = 1;
      frees_won = pos + 1; 
      freespinsb = frees_won;
      freespins += frees_won;
      freespins_total += frees_won;
      bonus_freespins += frees_won;
      freespins_left += frees_won;
      return 0;
    }
    else
    {
      /* Additional cash bonus */
      tmp = genrand_range(0, 99);
      pos = 0;
      do {
        if (tmp < bonus1_odds[pos])
          break;
        pos++;
      }while(pos < BONUS1_RANGE);
      bonus1_gain = (bonus1value[pos][0] + genrand_range(0, bonus1value[pos][1] - bonus1value[pos][0]));
      return 1;
    }
  }
  return -1;
}

/* Bonus 2 management */
void bonus2_generate(int i) {
  int tmp, pos;
  /* Additional cash bonus */
  tmp = genrand_range(0, 999);
  pos = 0;
  do {
    if (tmp < bonus2_odds[pos])
      break;
    pos++;
  }while(pos < BONUS2_RANGE);
  bonus2_gain[i] = (bonus2value[pos][0] + genrand_range(0, bonus2value[pos][1] - bonus2value[pos][0]));
}

/* ==================================================================================================
    UPDATE FREESPINS
================================================================================================== */
void update_freespins() {
  if (flag_freespins_won) {
    flag_freespins_on = 1;  // freespins session starting
    flag_freespins_won = 0;
  }
  if (flag_freespins_on && freespins_left >= 0)
    freespins_left--;
  if (freespins_left == -1) {
    freespins_left = 0;
    flag_freespins_on = 0;
    freespins_mult = 1;
    freespins_total = 0;
    freespins_gain = 0;
  }
}

/* ==================================================================================================
    BONUS CHOICE
================================================================================================== */
//for bonus1
void bonus1_choice() {
  int cmd1;
  cmd1 = read_cmd(0);
  if ((cmd1 < 0 || cmd1 > BONUSOPTIONS1) && (cmd1 != number_of_bonuses1))
    general_error(ERROR_INVALID_VALUE, "invalid nr of options");
  nr_of_options1 = cmd1;

  cmd1 = read_cmd(0);
  if (cmd1 < 0 || cmd1 > nr_of_options1)
    general_error(ERROR_INVALID_VALUE, "invalid nr of choices");
  nr_of_choices1 = cmd1;

  cmd1 = read_cmd(0);
  if (cmd1 < 0 || cmd1 > 1)
    general_error(ERROR_INVALID_VALUE, "invalid flag value ");
  show_all_bonus_flag1 = cmd1;
}

//for main bonus
void bonus2_choice() {
int cmd1, i;
  cmd1 = read_cmd(0);
  if (cmd1 < 0 || cmd1 > BONUSOPTIONS)
    general_error(ERROR_INVALID_VALUE, "invalid nr of options");
  nr_of_options2 = cmd1;

  cmd1 = read_cmd(0);
  if (cmd1 < 0 || cmd1 > bonus2value[0][0])
    general_error(ERROR_INVALID_VALUE, "invalid nr of choices");
  nr_of_choices2 = cmd1;

  cmd1 = read_cmd(0);
  if (cmd1 < 0 || cmd1 > 1)
    general_error(ERROR_INVALID_VALUE, "invalid flag value ");
  show_all_bonus_flag = cmd1;

  for (i = 0; i < nr_of_options2; ++i) {
    bonus2_generate(i);
    bonus2_winnings[i] = bonus2_gain[i];
  }
}

/* ==================================================================================================
    BONUS 1 ROUND
================================================================================================== */
void bonus1_round() {
  int cmd1, ret;
  cmd1 = read_cmd(0);
  if (cmd1 < 0 || cmd1 > 4)
    general_error(ERROR_INVALID_VALUE, "invalid bonus choice");
  if (bonus1_options[cmd1] != 0)
    general_error(ERROR_INVALID_VALUE, "already used this bonus choice");
  bonus1_count++;
  numbers_bonus1[cmd1] = bonus1_count;
  ret = bonus1_generate();

  if(ret == 0) { /* FS won */
    bonus1_winnings[cmd1] = 0;
    bonus1_fs_won[cmd1] = freespinsb;
    bonus_gain = 0;
    winnings_fs[cmd1] = 1;
    winnings_mult[cmd1] = 0;
  }
  else { /* Additional cash won */
    bonus1_fs_won[cmd1] = 0;
    bonus1_winnings[cmd1] = bonus1_gain;
    winnings_mult[cmd1] = 1;
    bonus_gain = bonus1_gain * number_of_coins * number_of_lines;
    total_gain += bonus1_winnings[cmd1] * number_of_coins * number_of_lines;
    total_bonus += bonus1_winnings[cmd1] * number_of_coins * number_of_lines;
  }

  if (bonus1_count == nr_of_choices1) {
    if(flag_freespins_on || flag_freespins_won) {
      freespins_gain += total_bonus;
      freespins_gain += partial_gain;
    }
    ready = 1;
    win(1);
  }
  else
    send_data(STATE_BONUS_WIN);
}

/* ==================================================================================================
    BONUS 2 ROUND
================================================================================================== */
void bonus2_round() {
  int cmd2;
  cmd2 = read_cmd(0);
  if (cmd2 < 0 || cmd2 > nr_of_options2)
    general_error(ERROR_INVALID_VALUE, "invalid bonus choice");

  bonus_count++;
  
  bonus2_position = genrand_range(0, 11);
  bonus2_options[bonus2_position] = 1;
  bonus_gain = bonus2_winnings[bonus2_position] * number_of_coins * number_of_lines;
  total_gain += bonus2_winnings[bonus2_position] * number_of_coins * number_of_lines;
  total_bonus2 += bonus2_winnings[bonus2_position] * number_of_coins * number_of_lines;

  if(flag_freespins_on) {
    freespins_gain += total_bonus2;
    freespins_gain += partial_gain;
  }

  sprintf(screen, "%s-*-B:%d", screen, bonus2_winnings[bonus2_position]);

  if (bonus_count == nr_of_choices2) {
    if(flag_freespins_on) {
      freespins_gain += total_bonus2;
      freespins_gain += partial_gain;
    }
    win(1);
  }
  else
    send_data(STATE_BONUS_WIN);
}
