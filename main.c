#include <isbc/log.h>
#include "slt_9l_astro.h"
#include "process_login.h"
#include "process_game.h"
#include "values_error_codes.h"

/******************************************
      MAIN
******************************************/
int main(int argc, char** argv)
{
    if (argc < 1)
        return -1;

    /* Game variables */
      // game variables ?

    /* Login sequence and initialization */
    int ret = processLogin(/* parameters ? */);
    if (ret != 0) {
        log_(LOG_ERROR, "(main.c) Error in processInit [%i]", ret);
    }

    /* Game sequence */
    ret = processGamePlay(/* parameters ? */);
    if (ret != 0) {
        log_(LOG_ERROR, "(main.c) Error in processGame [%i]", ret);
    }

    /* Clean sequence */
    //ret = processClean(/* parameters ? */);

    return 0;
}