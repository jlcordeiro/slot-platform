#include <isbc/log.h>
#include "init.h"


/* ==================================================================================================
    INITIALIZE VARS
================================================================================================== */
void init_vars() {
  int i, j;
  // init screen string for statistics
  memset(screen, 0, MAX);
  strcpy(screen, "-");
  // init generic variables
  memset(reel_pos, 0, 50);
  strcpy(reel_pos, "-");
  for (i=0; i<ROWS; i++)
    for (j=0; j<COLS; j++)
      combination[i][j] = -1;
  for (i=0; i<LINES; i++) {
    lines_gain[i] = 0;
    lines_gain_index[i] = -1;
  }
  number_of_scatters = 0;
  number_of_bonuses1 = 0;
  number_of_bonuses2 = 0;
  partial_gain = 0;
  for (i = 0; i < BONUSOPTIONS1; i++) {
    bonus1_winnings[i] = 0;
    numbers_bonus1[i] = 0;
    winnings_mult[i] = 0;
    winnings_fs[i] = 0;
    bonus1_fs_won[i] = 0;
  }
  for (i = 0; i < BONUSOPTIONS; i++) {
    bonus2_gain[i] = 0;
    bonus2_options[i] = 0;
    bonus2_winnings[i] = 0;
    bonus_parts_values[i] = 0;
  }
  total_gain = 0;
  bonus1_gain = 0;
  freespinsb = 0;
  ready = 0;
  total_bonus = 0;
  total_bonus2 = 0;
  bonus_freespins = 0;
  flag_bonus_won = 0;
  flag_bonus1_won = 0;
  flag_bonus2_won = 0;
  flag_freespins_won = 0;
  freespins = 0;
  doubles = 0;
  bonuses1 = 0;
  bonus_gain = 0;
  bonus_count = 0;
  bonus1_count = 0;
  nr_of_options1 = 0;
  limit = 0;
  for (i = 0; i < LINES; i++) {
    for (j=0; j<COLS; j++)
      highlight[i][j] = '0';
    highlight[i][COLS] = '\0';
  }
  for (i = 0; i < ROWS*COLS; i++) {
    highlight_bonus1[i] = '0';
    highlight_bonus2[i] = '0';
    highlight_scatter[i] ='0';
  }
  highlight_bonus1[ROWS*COLS] = '\0';
  highlight_bonus2[ROWS*COLS] = '\0';
  highlight_scatter[ROWS*COLS] = '\0';
}


