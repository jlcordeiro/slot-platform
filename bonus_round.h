#ifndef H_BONUSROUND
#define H_BONUSROUND
///\file

#include <stdio.h>
#include "slt_9l_astro.h"
#include "mechanics.h"
#include "../../headers/values_error_codes.h"

/**
 * \brief Executes the login process.
 * \param game game values
 * \param configuration config values
 * \return 0 if OK
 */

int genrand_range(int, int);
int read_cmd(int);
void general_error(char*, char*);
void send_data(int);
void win(int);

#endif
