#ifndef H_SLT9LASTRO
#define H_SLT9LASTRO
///\file

/* ==================================================================================================
		SERVER'S SPECIFIC VARS
===================================================================================================*/

// game ids
#define MACHINE_ID 2163			    // unique server id
#define GAME_ID 2163				// unique server id
#define GAME_TYPE 2					// server type, for statistics

// dmiensions
#define ROWS 3						// number of rows (elements displayed per reel)
#define COLS 5	

// slot with coins x lines bet
#define COINS_X_LINES 1

// lines
#define LINES 9					// number of paylines
int lines[LINES][COLS] = {      
  {1, 1, 1, 1, 1},   //1       
  {0, 0, 0, 0, 0},   //2       
  {2, 2, 2, 2, 2},   //3        
  {0, 1, 2, 1, 0},   //4       
  {2, 1, 0, 1, 2},   //5       
  {0, 0, 1, 0, 0},   //6
  {2, 2, 1, 2, 2},   //7
  {0, 1, 1, 1, 0},   //8
  {2, 1, 1, 1, 2}    //9
};

// coin values
#define COINS 8						// number of accepted coin values
#define COIN_DEFAULT 1				// default coin value
int coin_values[COINS] = {1, 2, 5, 10, 20, 50, 100, 200};		// list of accepted coin values

// bets
#define BETS 5

// double or collect
#define MAX_DOUBLE 5				// maximum number of doubles allowed
#define SAVED_COLOURS 12    // saved cards from the previous DorQ round


/* Strips */
#define STRIPS 2                // number of strips
#define MIN_SYMBOL_VALUE 1      // minimum symbol value
#define MAX_SYMBOL_VALUE 16     // maximum symbol value
#define MAX_REELS 173           // maximum reel size
int max_reel[STRIPS][COLS] = {
	{154,156,173,119,125},
	{ 149, 150, 159, 143, 144 }
};

#define FREESPINS         5
#define WILD_SYMB         13
#define SCATTER_SYMB      14
#define BONUS1_SYMB       15
#define BONUS2_SYMB       16
#define NB_NORM_SYMB      12
#define TAB_NB_NORM_SYMB  NB_NORM_SYMB + 1
#define NB_SPECIAL_SYMB   3
#define NB_WILD_SYMB      1
#define NB_TOTAL_SYMB     NB_NORM_SYMB + NB_SPECIAL_SYMB + NB_WILD_SYMB
#define TAB_NB_TOTAL_SYMB NB_TOTAL_SYMB + 1
#define MAX_SCATTERS      4
//paytable

/* Paytable */
int gain_table[13][5] = {
    {0,    0,    5,    10,    100},      //s1
    {0,    0,   10,    20,    100},      //s2
    {0,    0,   10,    20,    150},      //s3
    {0,    0,   15,    25,    150},      //s4
    {0,    0,   15,    50,    150},      //s5
    {0,    0,   20,    80,    200},      //s6
    {0,    0,   25,   150,    300},      //s7
    {0,   10,   25,   150,    400},      //s8
    {0,   10,   25,   150,    450},      //s9
    {0,   10,   25,   150,    500},      //s10
    {0,   20,   50,   150,    800},      //s11
    {0,   25,   50,   200,   1000},      //s12
    {2,   10,  100,   500,   1000}       //s13-wild
};

/* Special symbols : scatter, bonus1, bonus2 odds */
int special_odds[STRIPS][NB_SPECIAL_SYMB][COLS] = {
  { 
    {  30,  30,  48,  60,  60 },
    {  90, 102, 168, 210, 210 },
    { 150, 162, 258, 312, 330 }
  },
  { 
    {  60,  72,  60,  60,  60 },
    { 120, 132, 150, 210, 180 },
    { 180, 192, 300, 360, 330 }
  }
};

/* Scatter paytable */
int scatter_value[4] = {2, 5, 25, 200};

/* Wilds */
int wild_odds[STRIPS][COLS] = { 
  {  84,  81,  81, 12, 12 },
  { 120, 150, 120, 60, 60 }
};

/* Bonus 1 */
#define BONUS1_TYPES    2
#define BONUS1_RANGE    5
#define BONUS1_FS_RANGE 10
#define BONUSOPTIONS1 5
int bonus1_odds[BONUS1_RANGE] = {77,97,98,99,100};
int bonus1value[BONUS1_RANGE][2] = {
  {  5,   10},
  { 11,   15},
  { 16,   25},
  { 26,   40},
  { 41,   50}
};
int bonus1fs[BONUS1_FS_RANGE] = {  5, 15, 25, 45, 65, 75, 85, 90, 95, 100};

int bonus1_options[BONUSOPTIONS1] = {0,0,0,0,0};
int bonus1_winnings[BONUSOPTIONS1] = {0,0,0,0,0};
int bonus1_fs_won[BONUSOPTIONS1] = {0,0,0,0,0};

//for bonus 2

/* Bonus 2 */
#define BONUS2_RANGE 9
#define BONUSOPTIONS 12
int bonus2_odds[BONUS2_RANGE] = { 202, 502, 802, 912, 972, 994, 998, 999, 1000 };
int bonus2value[BONUS2_RANGE][2] = {
  {  15,  20 },
  {  21,  30 },
  {  31,  40 },
  {  41,  60 },
  {  61,  80 },
  {  81, 100 },
  { 101, 200 },
  { 201, 500 },
  { 501, 550 }
};
int bonus2_options[BONUSOPTIONS];
int bonus2_winnings[BONUSOPTIONS]; 

// client commands
#define CMD_LOGOUT			1		// player logs out
#define CMD_COIN			2		// player changes coin value
#define CMD_BET				3		// player starts a new game (with a bet)
#define CMD_ENTER_DOUBLE	5		// player enters double/collect screen (doesn't automatically choose to double)
#define CMD_DOUBLE			6		// player chooses to double
#define CMD_COLLECT			7		// player chooses to collect
#define CMD_ENTER_BONUS		8		// player enters bonus1 screen (doesn't automatically send a bonus value or option)
#define CMD_BONUS			9		// player chooses an option for bonus / sends the skill bonus value
#define CMD_DOQ_ENABLE		10		// player enables DoQ option
#define CMD_TEST          	770		// test mode for client 
#define CMD_PAUSE			777		// pause the server
#define CMD_STRIPS          771		// send the strips to client only in test mode

// server states
#define STATE_BEGIN		0	        // normal exit, a new game should start
#define STATE_INITIAL		1		// (re)start server, recover last screen
#define STATE_END_PHASE		2		// database problem while running end_phase
#define STATE_COPY_PHASE	3		// database problem while running copy_phase
#define STATE_UPDATE_GAIN	14		// database problem while refreshing player's money
#define STATE_GAIN			15		// database problem while adding money to client's account (result already generated)
#define STATE_BONUS_ROUND	16		// exit while waiting for player press the "enter bonus screen" button
#define STATE_BONUS_CHOICE	17		// exit while waiting for player to choose bonus option / invalid bonus option
#define STATE_BONUS_WIN		18		// (not a state but used to send data to client) money automatically collected after winning a bonus
#define STATE_END_GAME		19		// exit while waiting for player to enter double or collect in main screen
#define STATE_DOUBLE_CHOICE 20		// exit while waiting for player to double or collect in double/collect screen
#define STATE_MONEY_UPDATE	21		// (not a state) used to send updated money to client
#define STATE_LOST          22

// client states, for each server state defined above
char client_states[22][50] = {
	{"beginGame"},
	{"initial"},
	{"interrupted"},
	{"interrupted"},	
	{"interrupted"},
	{"interrupted"},
	{"interrupted"},	
	{"interrupted"},
	{"interrupted"},	
	{"interrupted"},
	{"interrupted"},
	{"interrupted"},	
	{"interrupted"},
	{"interrupted"},	
	{"interrupted"},
	{"doubleWin"},
	{"bonusRound"},
	{"bonusChoice"},
	{"bonusWin"},
	{"endGame"},
	{"doubleChoice"},
	{"updateMoney"}
};

/* ==================================================================================================
		GENERAL VARS
===================================================================================================*/

// generic constants
#define MAX 255								// generic string length
#define XMLMAX 32000						// xml text length
#define SSMAX 4096							// savescreen backup string length

// statistics
int skin_id = -1;							// unique id for clients
int user_id = -1;							// unique id for players
char paycard[MAX];							// player's casino
char screen[MAX];							// screen (cards, slot screen, etc) or error messages, for statistics
char screen_error[MAX];


// generic game variables
int flag_server_paused = 0;					// flag for pausing/resuming a server execution
int acc_return;								// which function that called savetp() with a bet was interrupted
int coin_value = COIN_DEFAULT;				// current coin value
int player_money;							// player's current money
int number_of_coins = BETS;					// player's current bet per line (in coins)
int number_of_lines = LINES;				// player's current number of lines
int combination[ROWS][COLS];				// symbol combination
int lines_gain[LINES];						// gain for each line (in coins)
int lines_gain_index[LINES];				// gain index for each line (see paytable)
int current_index;							// internal variable
int number_of_scatters;						// number of scatter symbols in the combination
int number_of_bonuses1;						// number of bonus1 symbols in the combination
int number_of_bonuses2; 					// number of bonus2 symbols in the combination
int partial_gain;							// player's gain without the bonus
int bonus1[3];								// number of bonuses1 (of each type)
int ready;
int last_colours[SAVED_COLOURS]={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
											// last cards from the DorQ round
int bonus_type;								// type of the bonus
int bonustypes = 0;
int bonus_gain =0;                         	 // bonus value
int bonus_count = 0;                       	 // counter for bonus options
int bonus1_count = 0;                        // counter for bonus options
int numbers_bonus1[BONUSOPTIONS1]; 
int winnings_bonus1[BONUSOPTIONS1];
int winnings_mult[BONUSOPTIONS1];
int winnings_fs[BONUSOPTIONS1];
int bonus_1[10];					           	
int bonus1_gain;
int bonus1_1_gain;
int bonus1_2_gain;
int bonus1_3_gain;
int bonus2_gain[12];							// player's real bonus gain
int total_gain;								// player's total gain
int slot_gain;								// player's total gain
int flag_bonus_won;							// marks if bonus is won
int flag_bonus1_won;						// marks if bonus1 is won
int flag_bonus2_won;						// marks if bonus2 is won
int flag_freespins_won;						// marks if freespins are won during current game
int flag_freespins_xml = 0;           // marks if freespins are won during current game for xml (delayed update)
int flag_freespins_on = 0;					// marks if current game is a freespin
int freespins_left;							// number of freespins left (not including current one)
int freespins_mult = 1;						// freespins multiplier
int freespins;								// total number of freespins won
int freespinsb;								// total number of freespins won
int freespins_total = 0;					// total number of uninterrupted freespins (if more freespins won during freespins)
int nmise_action_index = 0;					// the index of nmise actions in current session
int freespins_gain = 0;						// gain during freespins
int doubles;								// numbers of time the player doubled
int bonuses1;  								// numbers of time the player  discovered a bonus
int limit;									// flag for 5 consecutive doubles won
int bonus_parts_values[BONUSOPTIONS];       // parts of bonus
int total_bonus;							// total bonus gain
int total_bonus2;							// total bonus2 gain
int bonus_freespins; 						// total freespins bonus
int bonus1_cmd;
int nr_of_options2 = 1;                      // options for bonus
int nr_of_choices2 = 1;                      // number of choises allowed for bonus
int show_all_bonus_flag = 0;                // flag used to show or not all bonus values
int nr_of_options1 = 1;                	 	// options for bonus
int nr_of_choices1 = 1;                 	// number of choises allowed for bonus
int show_all_bonus_flag1= 0;            	// flag used to show or not all bonus valuess
int bonus1cmd[5];
int bonus2_position;
char highlight[LINES][COLS+1];				// symbol highlight mask for each line
char highlight_scatter[ROWS*COLS+1];		// scatters highlight mask for whole displayed combination
char highlight_bonus2[ROWS*COLS+1];			// bonus highlight mask for whole displayed combination
char highlight_bonus1[ROWS*COLS+1];			// mini bonus highlight mask for whole displayed combination
char data_buffer[XMLMAX*5];					// internal buffer for concatenating all xml-s between 2 reads into one big xml
char data_buffer_temp[XMLMAX*5];			// internal buffer for concatenating all xml-s between 2 reads into one big xml
int v[3]={25,25,50};
int options[15];
int bon_gain=0;
int flag_doq_on = 0;						// DoQ feature on/off
int flag_doq_change = 0;					// DoQ feature will be turned on/off from off/on 
// strips
int reels[STRIPS][COLS][MAX_REELS];			// the reels
char reel_pos[50];							// the positions where the reels stopped

// save screen
char ss_parameters[SSMAX];					// savescreen backup screen
int ss_state;								// server's current state


#endif
