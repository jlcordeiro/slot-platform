#ifndef H_PROCESSLOGIN
#define H_PROCESSLOGIN
///\file

#include <signal.h>
#include "init.h"
#include "slt_9l_astro.h"

#include "../../headers/mysql/headers.c"

/**
 * \brief Executes the login process.
 * \param game game values
 * \param configuration config values
 * \return 0 if OK
 */
int processLogin();

#endif
